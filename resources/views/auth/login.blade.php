@extends('layouts.app')
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
@section('tittle', 'Login')
@section('content')
<div class="container">
    @if ($errors->has('error'))
    <div class="alert alert-danger">
      <ul>
        <li>{{ $error }}</li>
      </ul>
    </div>
  @endif
  <section>       
    <div id="container_demo" >
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>
        <div id="wrapper">
          <div id="login" class="animate form">
            <form  method="POST" action="{{url('/user/login')}}" autocomplete="on"> 
              @csrf
              <h1>Log in Users</h1> 
                <p> 
                  <label for="nickname" class="uname"> Your Nickname </label>
                  <input id="nickname" name="nickname" required="required" type="text" placeholder="yourusername" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" value="{{ old('nickname') }} "/>
                  @if ($errors->has('nickname'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('nickname') }}</strong>
                    </span>
                  @endif
                </p>
                <br>
                <p class="login button"> 
                  <a href="{{url('session')}}">ENTRAR</a>
                </p>
                <p class="login button"> 
                  <input type="submit" value="Login" />
                </p>
                <p class="change_link">
                  <a href="#toregister" class="to_register">ADMINISTRATOR</a>
                </p>
            </form>
          </div>
          <div id="register" class="animate form">
            <form  method="POST" action="{{url('/admin/login')}}" autocomplete="on"> 
              @csrf
              <h1>Log in Administrator</h1> 
              <p> 
                <label for="adminname" class="uname">Your username</label>
                <input id="adminname" name="adminname" type="text" placeholder="yourusername" class="form-control{{ $errors->has('adminname') ? ' is-invalid' : '' }}" value="{{ old('adminname') }}"/>
                 @if ($errors->has('adminname'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('adminname') }}</strong>
                  </span>
                @endif
              </p>
              <p> 
                <label for="password" class="youpasswd">Your password </label>
                <input id="password" name="password" type="password" placeholder="************" value="{{ old('password') }}" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"/>
                 @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </p>
              <p class="signin button">
                <input type="submit" value="Login"/>
              </p>
              <p class="change_link">
                <a href="#tologin" class="to_register">USERS</a>
              </p>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
