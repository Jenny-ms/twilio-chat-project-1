@extends('layouts.app')
<link href="{{ asset('css/background.css') }}" rel="stylesheet">
@section('tittle', 'Channels')
@section('content')
<br>
<div class="container">
	<div class="col-sm-12">
		<h2>CANALES</h2><br>
		<a href="{{route('channels.create')}}" class="btn btn-success" style="right: 40px; position: absolute;">
			AGREGAR <i class="fas fa-plus-square"></i>
	    </a>
	    <br>
	    <br>
		<table class="table table-hover table-striped table-bordered" style="width: 98%">
			<thead>
				<tr style="background-color: gray">
					<th style="text-align: center;">SID</th>
					<th style="text-align: center;">NOMBRE</th>
					<th style="text-align: center;">FECHA DE CREACIÓN</th>
					<th style="text-align: center;">FECHA DE ACTUALIZACIÓN</th>
					<th style="text-align: center;" colspan="2">ACCIÓN</th>
				</tr>
			</thead>
			<tbody>
				@foreach($channels as $channel)
				<tr style="background-color: #BFBEBE">
					<td>{{$channel->sid}}</td>
					<td>{{$channel->friendlyName}}</td>
					<td>{{date_format($channel->dateCreated, 'd/m/Y')}}</td>
					<td>{{date_format($channel->dateUpdated, 'd/m/Y')}}</td>
					<td>
						<a href="{{route('channels.edit', $channel->sid) }}" class="btn btn-primary">
							<i class="fas fa-edit"></i>
	                    </a>
	                </td>
					<td>
						<form  method="POST" action="{{ route('channels.destroy', $channel->sid) }}">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="DELETE">
							<button class="btn btn-danger">
	                                <i class="fas fa-trash-alt"></i>
	                        </button>
						</form>
	                </td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endsection
</div>