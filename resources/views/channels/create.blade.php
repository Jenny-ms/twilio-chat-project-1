@extends('layouts.app')
<link href="{{ asset('css/background.css') }}" rel="stylesheet">
@section('tittle', 'Add new channel')
@section('content')
<br><br><br><br><br><br><br><br>
<div class="container" style="background-color: white; border-radius: 10px; border: solid 1px;">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <h2 class="text-center">Add Channel</h2><br>
        <form class="login-form text-center" method="POST" action="{{url('/channel/create')}}">
            <div class="row">
                <div class="col">
                  @csrf
                  <div class="first">
                    <div class="form-group">
                      <label for="nombre" class="text-uppercase">Nombre</label>
                      <input type="text" name="nombre" id="nombre" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('nombre') }}" required>
                       @if ($errors->has('nombre'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                      @endif
                      <br><br>
                      <button class="btn btn-primary">AGREGAR</button> 
                  </div>
                  </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection