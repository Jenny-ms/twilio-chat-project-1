@extends('layouts.app')
<link href="{{ asset('css/background.css') }}" rel="stylesheet">
@section('tittle', 'Update channel')
@section('content')
<div class="container" style="border-radius: 10px;">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <h2 class="text-center">Update Channel</h2><br>
        <form class="login-form" method="post" action="{{route('channel.update')}}" enctype="multipart/form-data">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <div class="row" style="background-color: white">
                <div class="col-md-9">
                  <div class="form-group">
                    <label>SID</label>
                    <input type="hidden" name="sid" value="{{ $channel->sid }}">
                    <input type="text" name="sidChannel" class="form-control" required placeholder="Código" value="{{ $channel->sid }}" disabled="disabled">
                  </div>
                  <div class="form-group">
                    <label>NOMBRE</label>
                    <input type="text" name="nombre" class="form-control" required value="{{ $channel->friendlyName }}">
                  </div>
                  <div class="form-group">
                    <label>FECHA DE CREACIÓN</label>
                    <input type="text" name="datecrea" class="form-control" required value="{{ date_format($channel->dateCreated, 'd/m/Y') }}" disabled="disabled">
                  </div>
                  <div class="form-group">
                    <label>FECHA DE ACTUALIZACIÓN</label>
                    <input type="text" name="dateact" class="form-control" required value="{{ date_format($channel->dateUpdated, 'd/m/Y') }}" disabled="disabled">
                  </div>
                </div>
            </div>
            <br>
            <button class="btn btn-primary" type="submit">
              MODIFICAR
            </button>

                
        </form>
    </div>
</div>
@endsection