@extends('layouts.app')
<link href="{{ asset('css/background.css') }}" rel="stylesheet">
<link href="{{ asset('css/msg.css') }}" rel="stylesheet">
@section('tittle', 'Messages')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
@section('content')
<div class="container-fluid h-100">
			<div class="row justify-content-center h-100">
				<div class="col-md-4 col-xl-3 chat"><div class="card mb-sm-3 mb-md-0 contacts_card">
					<div class="card-header" style="background-color: #001F3F">
						<div class="input-group">
							<input type="text" placeholder=" {{$channel->membersCount}} MEMBERS" name="" class="form-control" disabled="disabled" style="text-align: center;">
						</div>
					</div>
					<div class="card-body contacts_body">
						<ui class="contacts">
						<li class="disabled">
							@foreach($members as $member)
								<div class="d-flex bd-highlight">
									<div class="img_cont">
										<img src="https://cdn3.iconfinder.com/data/icons/user-with-laptop/100/user-laptop-512.png" class="rounded-circle user_img">	
									</div>
									<div class="user_info">
										<span>{{$member->identity}}</span>
										<p>{{$member->sid}}</p>
									</div>
							</div>
							@endforeach
						</li>
						</ui>
					</div>
					<div class="card-footer"></div>
				</div></div>
				<div class="col-md-8 col-xl-6 chat">
					<div class="card">
						<div class="card-header msg_head" style="background-color: #001F3F">
							<div class="d-flex bd-highlight">
								<div class="user_info" style="text-align: center;">
									<span>{{$channel->friendlyName}}</span>
									<p>{{$channel->messagesCount}} Messages</p>
								</div>
							</div>
							<form method="POST" action="{{url('/message/send')}}">
							@csrf
							<input type="hidden" name="sid" value="{{ $channel->sid }}">
								<button id="action_menu_btn" style="align-self: center" type="submit">
									<i class="fas fa-redo-alt"></i>
								</button>
						</form>
						</div>
						<div class="card-body msg_card_body" id="messages">
						@foreach($messages as $msg)
							@if($msg->from == Session::get('nickname'))
								<div class="d-flex justify-content-end mb-4">
									<div class="msg_cotainer_send">
										{{$msg->body}}
									</div>
								</div>
								<span class="msg_time_send">{{date_format($msg->dateCreated, 'd/m/Y | g:i A')}}</span><br>
							@else
								<label>{{$msg->from}}</label><br>
								<div class="d-flex justify-content-start mb-4">
									<div class="msg_cotainer">
										{{$msg->body}}
									</div>
								</div>
								<span class="msg_time">{{date_format($msg->dateCreated, 'd/m/Y | g:i A')}} </span><br>
							@endif	
						@endforeach
						</div>
						<form method="POST" action="{{url('/message/send')}}">
							@csrf
							<input type="hidden" name="sid" value="{{ $channel->sid }}">
							<div class="card-footer d-flex" style="background-color: #001F3F">
								<textarea name="body" class="form-control" placeholder="Type your message..."></textarea>
								<button class="btn btn-primary" style="align-self: center" type="submit">
									<i class="fas fa-location-arrow"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
@endsection
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#messages").animate({ scrollTop: $('#messages')[0].scrollHeight}, 1000);
	});
</script>