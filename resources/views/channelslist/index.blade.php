@extends('layouts.app')
<link href="{{ asset('css/background.css') }}" rel="stylesheet">
@section('tittle', 'Channels List')
@section('content')
<div class="container">
    <div class="row">
    	<div class="col-md-8">
    		<h1>LISTA DE CANALES</h1>
    		@foreach($channels as $channel)
			<div class="col-md-3 card">
				<br><br>
				<form method="post" action="{{route('channelslist.create') }}">
					@csrf
					<input type="hidden" name="sid" value="{{ $channel->sid }}">
					<div>
					<button  class="btn btn-success" type="submit">
						<h3 style="color: black"><b>{{$channel->friendlyName}}</b></h3> <img src="{{URL::asset('/img/chat.png')}}" width="130" height="100" class="img-responsive"/>
					<h5><b>UNIRSE</b></h5>
					</button> 
				</div>
				</form>
			</div>		
			@endforeach
    	</div>
    	@if(Session::has('usersid'))
    		<div class="col-md-4">
    			<div class="row">
    				<div class="col-md-12">
		    			<h1>MIS CANALES</h1>
		    			@foreach($userChannels as $userChannel)
		    				@foreach($channels as $channel)
			    				@if($userChannel->channelSid == $channel->sid)
			    				<div class="col-md-6 card">
									<br><br>
									<form method="post" action="{{route('channelslist.create') }}">
										@csrf
										<input type="hidden" name="sid" value="{{ $channel->sid }}">
										<div>
										<button  class="btn btn-success" type="submit">
											<h3 style="color: black"><b>{{$channel->friendlyName}}</b></h3> <img src="{{URL::asset('/img/chat.png')}}" width="130" height="100" class="img-responsive"/>
										<h5><b>ENTRAR</b></h5>
										</button> 
									</div>
									</form>
								</div>
								@endif
							@endforeach
		    			@endforeach
    				</div>
    			</div>
    		</div>
    	@endif
	</div>
</div>
@endsection