<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::post('admin/login', 'UserController@checkadmin');
Route::post('user/login', 'UserController@checkusers');
Route::get('session', 'UserController@session');
Route::resource('channels', 'ChannelsController');

Route::post('channel/create', 'ChannelsController@store');
Route::patch('channel/update', 'ChannelsController@update')->name('channel.update');
Route::delete('channel/delete', 'ChannelsController@destroy')->name('channel.destroy');

Route::get('messages/{sid}', 'MessagesController@index');
Route::get('channelslist', 'ChannelslistController@index');
Route::post('channelslist/create', 'ChannelslistController@store')->name('channelslist.create');
Route::post('message/send', 'MessagesController@store');

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
