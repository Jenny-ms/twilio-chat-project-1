<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Twilio\Rest\Client;

class ChannelsController extends Controller
{
    
    protected $sid;
    protected $token;

    public function __construct()
    {
        $this->sid = 'ACd00a327fb16cf8774eff2bed6e878174';
        $this->token = '061207aab9897f7f3b6b32f7ec6ee834';
    }
    /**
     * Display a listing of the channels.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        $channels = $twilio->chat->v2->services($services[0]->sid)->channels->read();
        return view('channels.index', compact('channels'));
    }

    /**
     * Show the form for creating a new channel.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('channels.create');
    }

    /**
     * create a new channel in a service.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'nombre'   => 'required'
        ]);
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        $channel = $twilio->chat->v2->services($services[0]->sid)->channels->create(array("friendlyName" => $request->get('nombre')));
        return redirect('channels');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified channel.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sid)
    {
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        $channels = $twilio->chat->v2->services($services[0]->sid)->channels->read();
        foreach ($channels as $record) {
            if ($record->sid == $sid) {
                $channel = $record;
            }
        }
        return view('channels.update', compact('channel'));
    }

    /**
     * Update the specified channel in a service.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        $channel = $twilio->chat->v2->services($services[0]->sid)->channels($request->get('sid'))->update(array("friendlyName" => $request->get('nombre')));
        return redirect('channels');
    }

    /**
     * Remove the specified channel from service.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($sid)
    {
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        $twilio->chat->v2->services($services[0]->sid)->channels($sid)->delete();
        return redirect('channels');
    }
}
