<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Session;

class ChannelslistController extends Controller
{
  
    protected $sid;
    protected $token;

    public function __construct()
    {
        $this->sid = 'ACd00a327fb16cf8774eff2bed6e878174';
        $this->token = '061207aab9897f7f3b6b32f7ec6ee834';
    }
      /**
     * Display a listing of the channels.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        $channels = array();
        $userChannels = array();
        if ($services) {
            $channels = $twilio->chat->v2->services($services[0]->sid)->channels->read();
            if (Session::has('usersid')) {
                $userChannels = $twilio->chat->v2->services($services[0]->sid)->users(Session::get('usersid'))->userChannels->read();
            }
        }
        return view('channelslist.index', compact('channels', 'userChannels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($sid)
    {
        //
    }


    /**
     * Send a member sid to show the mesagges.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Session::has('nickname')) {
            $twilio = new Client($this->sid, $this->token);
            $services = $twilio->chat->v2->services->read();
            if ($services) {
                $members = $twilio->chat->v2->services($services[0]->sid)
                                ->channels($request->get('sid'))
                                ->members
                                ->read();
                if ($members) {
                    foreach ($members as $record) {
                        if ($record->identity == Session::get('nickname')) {
                            Session(['sid' => $record->sid]);
                            return redirect('messages/'.$request->get('sid'));
                        }
                    }
                }
                $member = $twilio->chat->v2->services($services[0]->sid)->channels($request->get('sid'))->members->create(Session::get('nickname'));
                Session(['sid' => $member->sid]);
                return redirect('messages/'.$request->get('sid'));
            }
        } else {
            return redirect('');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
