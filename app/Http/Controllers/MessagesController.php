<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Session;

class MessagesController extends Controller
{
    protected $sid;
    protected $token;

    public function __construct()
    {
        $this->sid = 'ACd00a327fb16cf8774eff2bed6e878174';
        $this->token = '061207aab9897f7f3b6b32f7ec6ee834';
    }

    /**
     * Display a listing of messages on a channel.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($sid)
    {
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        $members = $twilio->chat->v2->services($services[0]->sid)->channels($sid)->members->read();
        $messages = $twilio->chat->v2->services($services[0]->sid)->channels($sid)->messages->read();
        $channels = $twilio->chat->v2->services($services[0]->sid)->channels->read();
        foreach ($channels as $record) {
            if ($record->sid == $sid) {
                $channel = $record;
            }
        }
        return view('messages.index', compact('members', 'messages', 'channel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Send messages to a channel.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'body' => 'required',
            'sid'  => 'required'
        ]);
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        $message = $twilio->chat->v2->services($services[0]->sid)->channels($request->get('sid'))->messages->create(array("body" => $request->get('body'), "from" => Session::get('nickname')));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sid)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
