<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Twilio\Rest\Client;

use Session;

class UserController extends Controller
{
    protected $sid;
    protected $token;

    public function __construct()
    {
        $this->sid = 'ACd00a327fb16cf8774eff2bed6e878174';
        $this->token = '061207aab9897f7f3b6b32f7ec6ee834';
    }

    public function index()
    {
        return view('auth.login');
    }

      /**
     * validate administrator information.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkadmin(Request $request)
    {
        $this->validate($request, [
        'adminname'   => 'required',
        'password'  => 'required'
        ]);
        if ($request->get('adminname') =='admin'  && $request->get('password') == 'admin30') {
            $twilio = new Client($this->sid, $this->token);
            $services = $twilio->chat->v2->services->read();
            if ($services) {
                return redirect('channels');
            } else {
                $service = $twilio->chat->v2->services->create("TwilioChat");
                return redirect('channels');
            }
        } else {
            return redirect('');
        }
    }
      /**
     * validate users information.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkusers(Request $request)
    {
        $this->validate($request, [
        'nickname'   => 'required'
        ]);
        $twilio = new Client($this->sid, $this->token);
        $services = $twilio->chat->v2->services->read();
        if ($services) {
            $users = $twilio->chat->v2->services($services[0]->sid)->users->read();
            if ($users) {
                foreach ($users as $record) {
                    if ($record->identity == $request->get('nickname')) {
                        Session(['nickname' => $request->get('nickname')]);
                        Session(['usersid' => $record->sid]);
                        return redirect('channelslist');
                    }
                }
            }
            $user = $twilio->chat->v2->services($services[0]->sid)->users->
            create($request->get('nickname'));
            Session(['nickname' => $request->get('nickname')]);
            Session(['usersid' => $user->sid]);
            return redirect('channelslist');
        }
        return back();
    }
      /**
     * Destroy sessions.
     *
     * @return \Illuminate\Http\Response
     */
    public function session()
    {
        Session::flush();
        return redirect('channelslist');
    }
}
